import java.util.Scanner;
public class Challangecalc {

    Scanner in = new Scanner(System.in);
    public static void main(String[] args) {

        Challangecalc object = new Challangecalc();
        object.menu();

    }
    public void menu(){

        System.out.println(" ");
        System.out.println("----------------------------");
        System.out.println("CALCULATOR HITUNG BIDANG ");
        System.out.println("----------------------------");
        System.out.println(" 1. Luas Segitiga ");
        System.out.println(" 2. Luas Bujur Sangkar ");
        System.out.println(" 3. Luas Lingkaran ");
        System.out.println(" 4. Luas Persegi Panjang ");
        System.out.println(" 5. Luas Bola  " );
        System.out.println(" 6. Volume Bola  ");
        System.out.println(" 0. Exit " );
        System.out.println("Silahkan Masukan Pilihan : ");

        int pilih = in.nextInt();

        switch(pilih){
            case 1 : {
                Luas3 luas = new Luas3();
                luas.Segi3();
                menu();
                break;
            }

            case 2 : {
                LuasBS luas = new LuasBS();
                luas.BujurS();
                menu();
                break;

            }

            case 3 : {
                Luasling luas = new Luasling();
                luas.Ling();
                menu();
                break;

            }

            case 4 : {
                luaspersegiP luas = new luaspersegiP();
                luas.Persegi4();
                menu();
                break;

            }

            case 5 : {
                luasBola luas = new luasBola();
                luas.Bola();
                menu();
                break;

            }

            case 6 : {
                volumeBola luas = new volumeBola();
                luas.Vbola();
                menu();
                break;

            }
            case 0 : { System.exit(0);

            }
            default : {
                System.err.println("Mohon Maaf Terjadi Kesalahan"); System.exit(0); }



        }




    }
}
